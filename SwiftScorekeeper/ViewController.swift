//
//  ViewController.swift
//  SwiftScorekeeper
//
//  Created by Kelson Vella on 9/12/17.
//  Copyright © 2017 Disre. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var Team1Lbl: UILabel!
    @IBOutlet var Team2Lbl: UILabel!
    
    @IBOutlet var team2Name: UITextField!
    @IBOutlet var team1Name: UITextField!
    
    @IBOutlet var Team1Stepper: UIStepper!
    @IBOutlet var Team2Stepper: UIStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        team2Name.delegate = self
        team1Name.delegate = self
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) ->Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func Team1StepperChanged(_ sender: Any) {
        let myString = String(format: "%.0f", Team1Stepper.value)
        Team1Lbl.text = myString
    }
    
    @IBAction func Team2StepperChanged(_ sender: Any) {
        let myString = String(format: "%.0f", Team2Stepper.value)
        Team2Lbl.text = myString
    }
    
    @IBAction func ResetBtnTouched(_ sender: Any) {
        Team1Stepper.value = 0
        Team2Stepper.value = 0
        Team1Lbl.text = "0"
        Team2Lbl.text = "0"
        team1Name.text = ""
        team2Name.text = ""
    }
    
    

}

